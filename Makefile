sources = countdown.c

build : $(sources) createbin
	gcc -Wall -std=c99 -pedantic-errors -o ./bin/countdown $(sources)

clean :
	rm -rf ./bin/

createbin :
	mkdir -p ./bin/
