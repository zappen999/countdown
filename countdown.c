/**
 * The purpose of this program is to implement a countdown timer.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#define SEC_DAY 86400
#define SEC_HOUR 3600
#define SEC_MIN 60

void PrintUsage (char* p_msg)
{
	/* Print additional message if provided */
	if (p_msg != NULL)
	{
		printf("%s\n", p_msg);
	}

	printf("\nUsage:\n");
	printf("  countdown 30s\n");
	printf("  countdown 1h30s\n");
	printf("  countdown 3d3h30s\n");
}

/**
 * Parses human time to seconds.
 * Supported input formats:
 * - 30s
 * - 5m30s
 * - 1h30s
 * - 1h5m30s
 *
 * Returns -1 on invalid human times.
 */
time_t ParseHumanTime (const char *input)
{
	time_t total = 0; /* Total seconds parsed and accumulated */
	int startIndex = -1; /* Start index of the time sequence */
	int i = 0;

	while (input[i] != '\0')
	{
		if (input[i] >= '0' && input[i] <= '9')
		{
			/* Its a number. Remember the start index if not already present. */
			if (startIndex == -1)
			{
				startIndex = i;
			}
			else
			{
				/* There was already a starting index, now all we care about is
				 * looking for the time unit. */
			}
		} else if (startIndex == -1) {
			/* Its not a number, and we don't have starting number, something
			 * isn't right... */
			return -1;
		} else {
			/* Its not a number, and we have a startIndex, this must be the time
			 * unit */
			switch (input[i])
			{
				case 'd':
					total += atoi(&input[startIndex]) * SEC_DAY;
					break;
				case 'h':
					total += atoi(&input[startIndex]) * SEC_HOUR;
					break;
				case 'm':
					total += atoi(&input[startIndex]) * SEC_MIN;
					break;
				case 's':
					total += atoi(&input[startIndex]);
					break;
				default:
					/* Invalid time unit */
					return -1;
			}

			startIndex = -1; /* Rinse and repeat (look for next time spec) */
		}

		i++;
	}

	return total;
}

/**
 * Converts seconds into human readable format.
 */
void SecondsToHumanTime (char* p_buf, time_t sec)
{
	int days = 0, hours = 0, minutes = 0;

	if (sec >= SEC_DAY)
	{
		/* Its 1 day or more. Count the days and subtract them from seconds */
		days = sec/SEC_DAY;
		sec = sec - (days * SEC_DAY);
	}

	if (sec >= SEC_HOUR)
	{
		/* Its 1 hour or more. Count the hours and subtract them from seconds */
		hours = sec/SEC_HOUR;
		sec = sec - (hours * SEC_HOUR);
	}

	if (sec >= SEC_MIN)
	{
		/* Its 1 min or more. Count the mins and subtract them from seconds */
		minutes = sec/SEC_MIN;
		sec = sec - (minutes * SEC_MIN);
	}

	/* Construct a string. */
	if (days > 0)
	{
		char dayBuf[20] = "";
		sprintf(dayBuf, "%d day%s ", days, days == 1 ? "" : "s");
		strcat(p_buf, dayBuf);
	}

	if (hours > 0)
	{
		char hourBuf[20] = "";
		sprintf(hourBuf, "%d hour%s ", hours, hours == 1 ? "" : "s");
		strcat(p_buf, hourBuf);
	}

	if (minutes > 0)
	{
		char minBuf[20] = "";
		sprintf(minBuf, "%d minute%s ", minutes, minutes == 1 ? "" : "s");
		strcat(p_buf, minBuf);
	}

	if (sec > 0)
	{
		char secBuf[20] = "";
		sprintf(secBuf, "%ld second%s ", sec, sec == 1 ? "" : "s");
		strcat(p_buf, secBuf);
	}
}

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		PrintUsage("You must provide one argument specifying time.");
		return 1;
	}

	time_t timerSeconds = ParseHumanTime(argv[1]);

	if (timerSeconds == -1)
	{
		PrintUsage("Invalid time unit.");
		return 1;
	}

	time_t start = time(NULL);
	time_t now = 0;
	time_t elapsed = 0;
	char humanTimeBuf[100] = "";

	while (now < start + timerSeconds - 1)
	{
		now = time(NULL);
		elapsed = now - start;
		humanTimeBuf[0] = '\0'; /* Clear the string from last use */
		SecondsToHumanTime(humanTimeBuf, timerSeconds - elapsed);
		printf("\r%-50s", humanTimeBuf);
		fflush(stdout);
		sleep(1);
	}

	printf("\rTime is up!\n");
	return 0;
}
